
/* On clicking on the Extension, total time spent on Facebook is printed in the logs */
chrome.browserAction.onClicked.addListener(function(){
var startDate, endDate;
chrome.storage.local.get("startTime", function(Time){ /* get start date */
var dateInString1 = Time.startTime.toString(); /*convert the object to string */
startDate = new Date(dateInString1); /*convert the string to date format */
console.log("Facebook started at " + startDate);

chrome.storage.local.get("endTime", function(Time2){ /* get end date */
var dateInString2 = Time2.endTime.toString();
endDate = new Date(dateInString2);
console.log("Facebook closed at " + endDate);
var Hdiff = parseInt(endDate.getHours()) - parseInt(startDate.getHours());
var Mdiff = parseInt(endDate.getMinutes()) - parseInt(startDate.getMinutes());
var Sdiff = parseInt(endDate.getSeconds()) - parseInt(startDate.getSeconds());
console.log("Total Time spent on Facebook today "+ Hdiff +"h " + Mdiff + "m");
});

});

});

/* The below code saves the start time when the Facebook was first accessed. */
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {

    if (changeInfo.status != 'complete')
        return;
    if (tab.url.indexOf('facebook') != -1) /* check if it is a facebook url */
	{
		chrome.storage.local.get('startTime', function(Time)
		{	
		if(typeof Time.startTime == 'undefined')
		{
			var a = new Date();
			var myDate = a.toString();
		    chrome.storage.local.set({'startTime': myDate}, function(Time){
			});
			
			
			}
		else 
			{	
			var a = new Date();		
			var myDate = a.toString();
			var storedTime;
			chrome.storage.local.get("startTime", function(Time) /* Retreive the stored date */
			{
			dateInString = Time.startTime.toString();
			storedTime = new Date(dateInString);
			if(a.getDate() != storedTime.getDate()) /* Checks if the date is the same, else reset the time */
			{ 
			chrome.storage.local.set({'startTime':myDate})
			}
			});	
			
			}
		});
	}
});

/* This code saves the time when the Facebook Tab was last closed */
chrome.tabs.onRemoved.addListener(function(tabId){
  
		chrome.storage.local.get('endTime', function(Time2)
		{
			var a = new Date();
			var endDate = a.toString();
		    chrome.storage.local.set({'endTime':endDate}, function(Time2){});
			
		});
		
		
			
		
});
